package com.live.hives.pkdemo

import android.Manifest
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.live.hives.pkdemo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding
    private var userId0 = 60000
    private var userId1 = 90000
    private var broadcastId0 = "100"
    private var broadcastId1 = "900"
    private val PERMISSION_ALL: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        activityMainBinding.button.setOnClickListener {
            startActivity(Intent(this, BroadcasterActivity::class.java).apply {
                putExtra(ARG_BROADCAST_LOCAL_ID_KEY, (broadcastId0.toInt() + 1).toString())
                putExtra(ARG_BROADCAST_REMOTE_ID_KEY, (broadcastId1.toInt() + 1).toString())
                putExtra(ARG_USER_LOCAL_ID_KEY, ++userId0)
                putExtra(ARG_USER_REMOTE_ID_KEY, ++userId1)
            })

        }

        activityMainBinding.button2.setOnClickListener {
            startActivity(Intent(this, BroadcasterActivity::class.java).apply {
                putExtra(ARG_BROADCAST_LOCAL_ID_KEY, (broadcastId1.toInt() + 1).toString())
                putExtra(ARG_BROADCAST_REMOTE_ID_KEY, (broadcastId0.toInt() + 1).toString())
                putExtra(ARG_USER_LOCAL_ID_KEY, ++userId1)
                putExtra(ARG_USER_REMOTE_ID_KEY, ++userId0)
            })

        }

        checkPermissions()

    }

    private fun checkPermissions() {
        val PERMISSIONS = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO
        )
        if (!PermissionUtils.hasPermissions(PERMISSIONS, this)) {
            ActivityCompat.requestPermissions(
                this,
                PERMISSIONS,
                PERMISSION_ALL
            )
        }

    }

}
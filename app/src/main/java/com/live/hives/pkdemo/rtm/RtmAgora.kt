package com.live.hives.pkdemo.rtm

import android.content.Context
import android.util.Log
import com.live.hives.pkdemo.R
import io.agora.rtm.RtmClient
import io.agora.rtm.RtmClientListener
import io.agora.rtm.RtmMessage
import io.agora.rtm.SendMessageOptions
import java.util.*

object RtmAgora {
    private val TAG = RtmAgora::class.java.simpleName
    var rtmClient: RtmClient? = null
    var sendMsgOptions: SendMessageOptions? = null
    private val listenerList: MutableList<RtmClientListener> = ArrayList()

    fun init(context: Context) {
        val appID = context.getString(R.string.private_app_id)
        try {
            rtmClient = RtmClient.createInstance(
                context.applicationContext,
                appID,
                object : RtmClientListener {

                    override fun onConnectionStateChanged(state: Int, reason: Int) {
                        for(listener in listenerList) {
                            listener.onConnectionStateChanged(state, reason)
                        }
                    }

                    override fun onMessageReceived(rtmMessage: RtmMessage, peerId: String) {
                        for(listener in listenerList) {
                            listener.onMessageReceived(rtmMessage, peerId)
                        }
                    }

                    override fun onTokenExpired() {

                    }

                    override fun onPeersOnlineStatusChanged(status: Map<String, Int>) {}
                })
            sendMsgOptions = SendMessageOptions()
            sendMsgOptions?.enableHistoricalMessaging = false
            // This setting applies to the peer-to-peer message only, not to the channel message.
            sendMsgOptions?.enableOfflineMessaging = false
        }
        catch(e: Exception) {
            Log.e(TAG, Log.getStackTraceString(e))
            throw RuntimeException(
                "NEED TO check rtm sdk init fatal error\n" + Log.getStackTraceString(
                    e
                )
            )
        }
    }

    fun registerListener(listener: RtmClientListener) {
        listenerList.add(listener)
    }

    fun unregisterListener(listener: RtmClientListener) {
        listenerList.remove(listener)
    }
}
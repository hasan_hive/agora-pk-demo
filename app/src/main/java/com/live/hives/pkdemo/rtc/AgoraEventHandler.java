package com.live.hives.pkdemo.rtc;

import java.util.ArrayList;
import java.util.List;

import io.agora.rtc.IRtcEngineEventHandler;

public class AgoraEventHandler extends IRtcEngineEventHandler {
    private List<RtcEngineEventHandler> handlers = new ArrayList<>();

    public void addHandler(RtcEngineEventHandler handler) {
        handlers.add(handler);
    }

    public void removeHandler(RtcEngineEventHandler handler) {
        handlers.remove(handler);
    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onJoinChannelSuccess(channel, uid, elapsed);
        }
    }

    @Override
    public void onLeaveChannel(RtcStats stats) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onLeaveChannel(stats);
        }
    }

    @Override
    public void onFirstRemoteVideoDecoded(int uid, int width, int height, int elapsed) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onFirstRemoteVideoDecoded(uid, width, height, elapsed);
        }
    }

    @Override
    public void onFirstRemoteVideoFrame(int uid, int width, int height, int elapsed) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onFirstRemoteVideoFrame(uid, width, height, elapsed);
        }
    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onUserJoined(uid, elapsed);
        }
    }

    @Override
    public void onUserOffline(int uid, int reason) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onUserOffline(uid, reason);
        }
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onLocalVideoStats(stats);
        }
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onRtcStats(stats);
        }
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onNetworkQuality(uid, txQuality, rxQuality);
        }
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onRemoteVideoStats(stats);
        }
    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onRemoteAudioStats(stats);
        }
    }

    @Override
    public void onLastmileQuality(int quality) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onLastmileQuality(quality);
        }
    }

    @Override
    public void onLastmileProbeResult(IRtcEngineEventHandler.LastmileProbeResult result) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onLastmileProbeResult(result);
        }
    }

    @Override
    public void onChannelMediaRelayStateChanged(int state, int code) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onChannelMediaRelayStateChanged(state, code);
        }
    }

    @Override
    public void onChannelMediaRelayEvent(int code) {
        for (RtcEngineEventHandler handler : handlers) {
            handler.onChannelMediaRelayEvent(code);
        }
    }
}

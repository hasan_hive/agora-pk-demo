@file:JvmName("Constants")

package com.live.hives.pkdemo

const val ARG_BROADCAST_LOCAL_ID_KEY = "broadcastIdLocal"
const val ARG_BROADCAST_REMOTE_ID_KEY = "broadcastIdRemote"
const val ARG_USER_LOCAL_ID_KEY = "userIdLocal"
const val ARG_USER_REMOTE_ID_KEY = "userIdRemote"
package com.live.hives.pkdemo.rtm

import org.json.JSONObject

object RTMUtil {

    fun createPkRequestPeerMessage(): JSONObject? {
        return try {
            val obj = JSONObject()
            obj.put("type", 0)
            obj
        }
        catch(e: Exception) {
            null
        }
    }
}

package com.live.hives.pkdemo.rtc

import android.content.Context
import com.live.hives.pkdemo.R
import io.agora.rtc.Constants
import io.agora.rtc.RtcEngine

/**
 * Singleton RtcEngine
 */
object RtcAgora {
    lateinit var rtcEngine: RtcEngine
    val agoraEventHandler = AgoraEventHandler()

    fun create(
        context: Context,
        enableVideo: Boolean
    ) {
        try {
            rtcEngine = RtcEngine.create(
                context.applicationContext,
                context.getString(R.string.private_app_id),
                agoraEventHandler
            )
            // Sets the channel profile of the Agora RtcEngine.
            // The Agora RtcEngine differentiates channel profiles and applies different optimization algorithms accordingly.
            // For example, it prioritizes smoothness and low latency for a video call, and prioritizes video quality for a video broadcast.
            rtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING)
            if(enableVideo) {
                rtcEngine.enableVideo()
            }
            else {
                rtcEngine.disableVideo()
            }
            // rtcEngine.setLogFile(FileUtil.initializeLogFile(context))
        }
        catch(e: Exception) {
        }
    }

    fun registerEventHandler(handler: RtcEngineEventHandler) {
        agoraEventHandler.addHandler(handler)
    }

    fun removeEventHandler(handler: RtcEngineEventHandler) {
        agoraEventHandler.removeHandler(handler)
    }

    fun destroy() {
        RtcEngine.destroy()
    }
}
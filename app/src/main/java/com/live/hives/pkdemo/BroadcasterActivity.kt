package com.live.hives.pkdemo

import android.os.Bundle
import android.util.Log
import android.view.SurfaceView
import android.view.View
import androidx.databinding.DataBindingUtil
import com.live.hives.pkdemo.databinding.ActivityBroadcasterBinding
import com.live.hives.pkdemo.rtc.RtcAgora
import com.live.hives.pkdemo.rtm.RTMUtil
import com.live.hives.pkdemo.rtm.RtmAgora
import io.agora.rtc.Constants
import io.agora.rtc.video.ChannelMediaInfo
import io.agora.rtc.video.ChannelMediaRelayConfiguration
import io.agora.rtc.video.VideoEncoderConfiguration
import io.agora.rtm.*
import org.json.JSONObject

class BroadcasterActivity : BaseRtcActivity() {
    private val TAG = BroadcasterActivity::class.java.simpleName
    private lateinit var activityBroadcasterBinding: ActivityBroadcasterBinding
    private var localSurfaceView: SurfaceView? = null
    private var localPKSurfaceView: SurfaceView? = null
    private var remotePKSurfaceView: SurfaceView? = null
    private var userId = 0
    private var userIdRemote = 0
    private var broadcastId = "0"
    private var broadcastIdRemote = "0"
    private var pkAgoraId = 0
    private var rtmChannel: RtmChannel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBroadcasterBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_broadcaster
        )
        getData()
        initUI()
        initRTC()
    }

    override fun onBackPressed() {
        onCloseClick()
    }

    override fun onDestroy() {
        stopMediaRelay()
        RtmAgora.rtmClient?.logout(null)
        leaveAndReleaseChannel()
        removeRtcEventHandler(this)
        unregisterRtmClientListener(this)
        RtcAgora.destroy()
        super.onDestroy()
    }

    override fun onJoinChannelSuccess(channel: String?, uid: Int, elapsed: Int) {
        initRtm()
        Log.d(
            TAG,
            "onJoinChannelSuccess uid " + uid
        )
    }

    override fun onUserJoined(uid: Int, elapsed: Int) {
    }

    override fun onUserOffline(uid: Int, reason: Int) {
    }

    override fun onFirstRemoteVideoDecoded(uid: Int, width: Int, height: Int, elapsed: Int) {
        runOnUiThread {
            renderPKBroadcast(uid)
            Log.d(
                TAG,
                "onFirstRemoteVideoDecoded uid " + uid
            )
        }
    }

    override fun onFirstRemoteVideoFrame(uid: Int, width: Int, height: Int, elapsed: Int) {
        Log.d(
            TAG,
            "onFirstRemoteVideoFrame uid " + uid
        )
    }

    override fun onChannelMediaRelayStateChanged(state: Int, code: Int) {
        runOnUiThread {
            Log.d(
                TAG,
                "onChannelMediaRelayStateChanged state " + state + "\n"
                    + "code " + code
            )
        }
    }

    override fun onChannelMediaRelayEvent(code: Int) {
        runOnUiThread {
            Log.d(
                TAG,
                "onChannelMediaRelayEvent code $code"
            )
        }
    }

    // peer msg
    override fun onMessageReceived(message: RtmMessage, peerId: String) {
        try {
            val msgObj = JSONObject(message.text)
            val type = msgObj.getInt("type")
            when(type) {
                0 -> {
                    runOnUiThread {
                        onPkRequestAcceptMessage(msgObj)
                    }
                }
            }
        }
        catch(e: Exception) {
            e.message
        }
    }

    override fun onTokenExpired() {}

    override fun onPeersOnlineStatusChanged(map: Map<String, Int>) {}

    override fun onMemberCountUpdated(i: Int) {}

    override fun onAttributesUpdated(list: List<RtmChannelAttribute?>) {}

    // channel msg
    override fun onMessageReceived(message: RtmMessage, fromMember: RtmChannelMember) {

    }

    override fun onMemberJoined(member: RtmChannelMember?) {

    }

    override fun onMemberLeft(member: RtmChannelMember?) {

    }

    private fun createPkRequest() {
        val rtmMessage = RtmAgora.rtmClient?.createMessage()
        rtmMessage?.text = RTMUtil.createPkRequestPeerMessage().toString()
        sendPeerPKRequestMessage(userIdRemote.toString(), rtmMessage)
    }

    private fun onPkRequestAcceptMessage(json: JSONObject) {
        startMediaRelay()
        setupPKUIMode()
        startPKBroadcast()
    }

    private fun getData() {
        broadcastId = intent.getStringExtra(ARG_BROADCAST_LOCAL_ID_KEY) ?: ""
        broadcastIdRemote = intent.getStringExtra(ARG_BROADCAST_REMOTE_ID_KEY) ?: ""
        userId = intent.getIntExtra(ARG_USER_LOCAL_ID_KEY, 0)
        userIdRemote = intent.getIntExtra(ARG_USER_REMOTE_ID_KEY, 0)
        Log.d(
            TAG,
            "onFirstRemoteVideoDecoded broadcastId "+broadcastId
        )
        Log.d(
            TAG,
            "onFirstRemoteVideoDecoded broadcastIdRemote "+broadcastIdRemote
        )
        Log.d(
            TAG,
            "onFirstRemoteVideoDecoded userId "+userId
        )
        Log.d(
            TAG,
            "onFirstRemoteVideoDecoded userIdRemote "+userIdRemote
        )

    }

    private fun initUI() {
        activityBroadcasterBinding.pkButton.setOnClickListener {
            createPkRequest()
        }
    }

    private fun sendPeerPKRequestMessage(
        peerId: String,
        message: RtmMessage?
    ) {
        if(message == null) {
            return
        }
        RtmAgora.rtmClient?.sendMessageToPeer(
            peerId,
            message,
            RtmAgora.sendMsgOptions,
            object : ResultCallback<Void?> {

                override fun onSuccess(aVoid: Void?) {
                    runOnUiThread {
                        startMediaRelay()
                        setupPKUIMode()
                        startPKBroadcast()
                    }
                }

                override fun onFailure(errorInfo: ErrorInfo) {
                    // refer to RtmStatusCode.PeerMessageState for the message state
                    val errorCode = errorInfo.errorCode
                    runOnUiThread {
                        when(errorCode) {
                            RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_TIMEOUT, RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_FAILURE ->
                            Log.d(
                                TAG,
                                "PEER_MESSAGE_ERR_TIMEOUT"
                            )

                            RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_PEER_UNREACHABLE ->
                            Log.d(
                                TAG,
                                "PEER_MESSAGE_ERR_PEER_UNREACHABLE"
                            )

                            RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_CACHED_BY_SERVER ->
                            Log.d(
                                TAG,
                                "PEER_MESSAGE_ERR_PEER_UNREACHABLE"
                            )
                        }
                    }
                }
            })
    }

    private fun createAndJoinChannel() {
        // step 1: create a channel instance
        rtmChannel = RtmAgora.rtmClient?.createChannel(
            broadcastId,
            this
        )
        if(rtmChannel == null) {
            return
        }

        // step 2: join the channel
        rtmChannel?.join(object : ResultCallback<Void?> {

            override fun onSuccess(responseInfo: Void?) {
                Log.i(TAG, "join channel success")
                getChannelMemberList()
            }

            override fun onFailure(errorInfo: ErrorInfo) {
                Log.e(TAG, "join channel failed")
            }
        })
    }

    /**
     * API CALL: get channel member list
     */
    private fun getChannelMemberList() {
        rtmChannel?.getMembers(object :
            ResultCallback<List<RtmChannelMember?>> {

            override fun onSuccess(responseInfo: List<RtmChannelMember?>) {
                runOnUiThread {
                    Log.d(
                        TAG,
                        "getMembers channelMemberCount: $responseInfo.size"
                    )
                }
            }

            override fun onFailure(errorInfo: ErrorInfo) {
                Log.e(TAG, "failed to get channel members, err: " + errorInfo.errorCode)
            }
        })
    }

    /**
     * API CALL: leave and release channel
     */
    private fun leaveAndReleaseChannel() {
        if(rtmChannel != null) {
            rtmChannel?.leave(null)
            rtmChannel?.release()
            rtmChannel = null
        }
    }

    private fun initRtm() {
        RtmAgora.init(this)
        RtmAgora.rtmClient?.login(null, userId.toString(), object :
            ResultCallback<Void> {

            override fun onSuccess(response: Void?) {
                createAndJoinChannel()
            }

            override fun onFailure(errorInfo: ErrorInfo?) {
                Log.e(TAG, "onFailure " + errorInfo?.errorDescription)
            }

        })
        registerRtmClientListener(this)
    }

    private fun onCloseClick() {
        finish()
    }

    private fun startMediaRelay() {
        val channelMediaInfoSrc = ChannelMediaInfo(broadcastId, null, 0)
        val channelMediaInfoDest = ChannelMediaInfo(broadcastIdRemote, null, 0)
        val relayConfig = ChannelMediaRelayConfiguration()
        relayConfig.setSrcChannelInfo(channelMediaInfoSrc)
        relayConfig.setDestChannelInfo(broadcastIdRemote, channelMediaInfoDest)
        val ret = RtcAgora.rtcEngine.startChannelMediaRelay(relayConfig)
        Log.d(
            TAG,
            "srcChannelName " + broadcastId + "\n"
                + "srcId " + "0"
        )
        Log.d(
            TAG,
            "destChannelName " + broadcastIdRemote + "\n"
                + "destId " + "0"
        )
        Log.d(
            TAG,
            "startChannelMediaRelay ret " + ret
        )
    }

    private fun stopMediaRelay() {
        RtcAgora.rtcEngine.stopChannelMediaRelay()
    }

    private fun initRTC() {
        RtcAgora.create(
            this,
            true
        )
        registerRtcEventHandler(this)
        joinChannel(
            null,
            broadcastId,
            userId
        )
        configVideo(
            VideoEncoderConfiguration.VD_640x480,
            VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_30,
            VideoEncoderConfiguration.STANDARD_BITRATE,
            VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT,
            Constants.VIDEO_MIRROR_MODE_AUTO,
        )
        startBroadcast()
        RtcAgora.rtcEngine.enableLocalVideo(true)
    }

    private fun startBroadcast() {
        RtcAgora.rtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER)
        localSurfaceView = prepareLocalVideo(userId)
        activityBroadcasterBinding.frameMain.addView(localSurfaceView)
    }

    private fun startPKBroadcast() {
        localPKSurfaceView = prepareLocalVideo(userId)
        activityBroadcasterBinding.pkHostFrameLayout.addView(localPKSurfaceView)
    }

    private fun renderPKBroadcast(uid: Int) {
        remotePKSurfaceView = prepareRemoteVideo(uid)
        activityBroadcasterBinding.participantPKFrameLayout.addView(remotePKSurfaceView)
        pkAgoraId = uid
    }

    private fun setupPKUIMode() {
        activityBroadcasterBinding.frameMain.visibility = View.GONE
        activityBroadcasterBinding.pkRoot.visibility = View.VISIBLE
        activityBroadcasterBinding.pkButton.visibility = View.GONE
    }
}
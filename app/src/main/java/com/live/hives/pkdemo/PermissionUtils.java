package com.live.hives.pkdemo;

import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class PermissionUtils {

    public static boolean hasPermissions(String[] permissions, AppCompatActivity activity) {
        if(permissions != null) {
            for(String permission : permissions) {
                if(ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
